require import AllCore FMap.
require (*--*) NBSES.

pragma +implicits.

type key, ptxt, nonce, ctxt.

clone include NBSES with
  type key   <- key,
  type ptxt  <- ptxt,
  type ctxt  <- ctxt,
  type nonce <- nonce.

(** Support for variable-length plaintexts **)
op length : ptxt -> int.
op dctxt : int -> ctxt distr.

axiom ge0_length p : 0 <= length p.

op dkey : key distr.

(** Injective indexed maps: we encode as maps of pairs.
    FIES returns the first element of the pair if the second is equal to some value. **)
op fies ['a, 'b] b (ab : ('a * 'b)) =
  if ab.`2 = b then Some ab.`1 else None.

op fieso b = obind (fies<:'a, 'b> b).

module type SAE_out = {
  proc gen()                 : unit
  proc enc(_ : ptxt * nonce) : ctxt option
  proc dec(_ : ctxt * nonce) : ptxt option
}.

module type SAE (E : NBSES) = {
  include SAE_out
}.

module SAEb = {
  var log : (nonce, ptxt * ctxt) fmap
  var k   : key option
}.

module (SAE0 : SAE) (E : NBSES) = {
  proc gen() = {
    var k0;

    k0 <$ dkey;
    if (SAEb.k = None) {
      SAEb.k <- Some k0;
    }
  }

  proc enc(m, n) = {
    var c;
    var co <- None;

    if (SAEb.k <> None /\ SAEb.log.[n] = None) {
                 c <@ E.enc(m, n, oget SAEb.k);
                co <- Some c;
      SAEb.log.[n] <- (m ,c);
    }
    return co;
  }

  proc dec(c, n) = {
    var m <- None;

    if (SAEb.k <> None) {
      m <@ E.dec(c, n, oget SAEb.k);
    }
    return m;
  }   
}.

module (SAE1 : SAE) (E : NBSES) = {
  proc gen() = {
    var ko;

    ko <$ dkey;
    if (SAEb.k = None) {
      SAEb.k <- Some ko;
    }
  }

  proc enc(m, n) = {
    var c;
    var co <- None;

    if (SAEb.k <> None /\ SAEb.log.[n] = None) {
                 c <$ dctxt (length m);
                co <- Some c;
      SAEb.log.[n] <- (m ,c);
    }
    return co;
  }

  proc dec(c, n) = {
    var m <- None;

    if (SAEb.k <> None) {
      m <- fieso c SAEb.log.[n];
    }
    return m;
  }   
}.

module type GSAE_out = {
  include SAE_out
}.

module type GSAE (E : NBSES) = {
  include GSAE_out
}.

module type D_GSAE (G : GSAE_out) = {
  proc run(): bool
}.

module GSAEb (AE : SAE) (E : NBSES) : GSAE_out = {
  include AE(E)
}.

module GSAE0 = GSAEb(SAE0).
module GSAE1 = GSAEb(SAE1).
