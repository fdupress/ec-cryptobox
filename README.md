To check the proofs, either install EasyCrypt's `r2022.04` release (following
instructions at https://github.com/easycrypt/easycrypt, and pinning to the
release tag), or use the release Docker container
`docker.io/easycryptpa/ec-test-box:latest`.

You can then run `make check` (or `docker run -v$PWD:/ssp
docker.io/easycryptpa/ec-test-box:latest sh -c "cd /ssp; make check"`) to
confirm that all proofs validate.

- File KEY defines the KEY packages;
- File PKEY defines the PKEY packages, and proves the statistical switching
  lemma (Lemma 2);
- File NBSES defines the syntax of nonce-based symmetric encryption schemes;
- File SAE defines the single-instance AE package;
- File AE defines the multi-instance AE package and formalizes the hybrid
  argument (Lemma 5);
- File NIKE defines the NIKE package and corresponding game;
- File Cryptobox defines cryptobox, mod-pkae and the various reductions,
  formalizes the core SSP (Theorem 1), and glues all parts of the proof
  together (Theorem 2).

### Troubleshooting

- If you are running the container using `podman` in rootless mode (generally
  preventing write-access into the work directory from the container), you may
  find it useful to modify the `Makefile` to define environment variable
  `ECARGS='-no-eco'`. This disables the incremental verification features,
  which require write access to the file system.
